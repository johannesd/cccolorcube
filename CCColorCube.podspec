Pod::Spec.new do |s|
  s.name         = "CCColorCube"
  s.version      = "0.0.1"
  s.summary      = "Color Cube"
  s.description  = <<-DESC
    Color Cube
                   DESC
  s.homepage     = "https://bitbucket.org/johannesd/cccolorcube"
  s.license      = { 
    :type => 'Custom permissive license',
    :text => <<-LICENSE
          Free for commercial use and redistribution. No warranty.

        	Johannes Dörr
        	mail@johannesdoerr.de
    LICENSE
  }
  s.author       = { "Johannes Doerr" => "mail@johannesdoerr.de" }
  s.platform     = :ios, '5.0'
  s.source       = { :git => "https://bitbucket.org/johannesd/cccolorcube" }
  s.source_files  = '*.{h,m}'

  s.exclude_files = 'Classes/Exclude'
  s.requires_arc = true
end
